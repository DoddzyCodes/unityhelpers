﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using System.Collections.Generic;

namespace DoddzyCodes
{
public class StateMachine {
        public delegate void StateDelegate();
        public delegate int StateTransitionCheckDelegate();

        private class StateInfo
        {
            public int StateID;
            public StateDelegate onStateEnter;
            public StateDelegate onStateExit;
            public StateDelegate onExecute;

            public StateTransitionCheckDelegate checkStateTransition;
        }
        

        private Dictionary<int, StateInfo> _registeredStates;
        StateInfo _currentState;

        public StateMachine()
        {
            _registeredStates = new Dictionary<int, StateInfo>();
            _currentState = null;
        }

        public void AddState(int stateID, StateDelegate executeDelegate, StateTransitionCheckDelegate transitionDelegate, StateDelegate enterDelegate = null, StateDelegate exitDelegate = null)
        {
            _registeredStates[stateID] = new StateInfo { StateID = stateID, onStateEnter = enterDelegate, onStateExit = exitDelegate, onExecute = executeDelegate, checkStateTransition = transitionDelegate };
        }

        public void SetCurrentState(int stateID, bool callStateEnter = true)
        {
            Assert.IsTrue(_registeredStates.ContainsKey(stateID), "Attempted to set invalid state.  Register a state with ID first");
            DoSwitchState(stateID, callStateEnter);
        }

        public int GetCurrentStateID() { return _currentState.StateID; }

        public void Execute()
        {
            if (_currentState != null)
            {
                _currentState.onExecute();
                int newState = _currentState.checkStateTransition();

                if (newState != _currentState.StateID)
                    DoSwitchState(newState);
            }
        }

        private void DoSwitchState(int stateID, bool callStateEnter = true)
        {
            Assert.IsTrue(_registeredStates.ContainsKey(stateID), "Attempted to switch to invalid state.  Register a state with ID first");

            if(_currentState != null && _currentState.onStateExit != null)
                _currentState.onStateExit();

            _currentState = _registeredStates[stateID];

            if(_currentState.onStateEnter != null && callStateEnter)
                _currentState.onStateEnter();
        }

    }


}


