﻿using UnityEngine;
   
namespace DoddzyCodes
{
    public static class GameObjectExtentions
    {
        public static T GetRequiredComponent<T>(this GameObject obj) where T : Component
        {
            T component = obj.GetComponent<T>();

            if (component == null)
            {
                Debug.LogError("Expected to find component of type "
                   + typeof(T) + " but found none", obj);
            }

            return component;
        }

        public static T GetRequiredComponentInChildren<T>(this GameObject obj) where T : Component
        {
            T component = obj.GetComponentInChildren<T>();

            if (component == null)
            {
                Debug.LogError("Expected to find component of type "
                   + typeof(T) + " in child hierarchy, but found none", obj);
            }

            return component;
        }
    }
}

